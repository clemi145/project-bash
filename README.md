# Project Bash

ProjectBash is a project of the HTL Rennweg (Vienna, Austria).   
The goal of this project is to create a web-based, emulated Unix-like terminal with a focus on getting to know the native bash commands.

## The Team

__*Product Owner*__ Clemens Jurkowitsch

__*Scrum Master*__ Markus Schmatzer

__*Team Members*__ Mario Seidl and Moritz Draskovits

## Implementation

### The Plan

We plan to build on top of the already existing project [LTerm](https://github.com/sr6033/lterm "LTerm").   
The planned expansions will include designing a web-view to display pre-defined exercises, where each one will focus on another aspect of Bash use cases.   
   
The exercises will each be linked to a Module on the website [SKiLLDiSPLAY](https://www.skilldisplay.eu/de/ "SKiLLDiSPLAY").   
That way people (especially students) can verify their knowledge by accomplishing the goals set by our application.

### The Goal

Our goal by developing this website is to give everyone with an internet connection an opportunity to learn about using one of the most powerful tools when working in Unix-based systems; the Bash.

### TODO

* [ ] Create Web-view around the terminal
* [ ] Implement communication with the SKiLLDiSPLAY platform via the provided REST API
* [ ] Expand the already existing commands to work more like they do in actual UNIX systems
* [ ] Expand the existing "filesystem" by simulating an actual UNIX folder-structure
